#!/bin/bash

g++  `root-config --cflags`  `root-config --libs`  -o Analysisscript_abcd Analysisscript_abcd.C

#SIGNAL
wtmca="1"
dsid="515624"
for file in /eos/user/m/marvin/SVJ/L65-sig/mca/$dsid/*.root; do
    string=$(echo "$file" | rev | cut -d"/" -f3 | rev | cut -d"/" -f3) a=$(echo "$file" | cut -d"." -f3)
    name=$(echo "$file" | rev | cut -d"/" -f4 | rev | cut -d"-" -f2)
    ./Analysisscript_abcd $dsid $wtmca $file $string $a $name
done

echo "============STARTING MCD=============="

wtmcd="1"
for file in /eos/user/m/marvin/SVJ/L65-sig/mcd/$dsid/*.root; do
    string=$(echo "$file" | rev | cut -d"/" -f3 | rev | cut -d"/" -f3) a=$(echo "$file" | cut -d"." -f3)
    name=$(echo "$file" | rev | cut -d"/" -f4 | rev | cut -d"-" -f2)
    ./Analysisscript_abcd $dsid $wtmcd $file $string $a $name
done

echo "============STARTING MCE=============="

wtmce="1"
for file in /eos/user/m/marvin/SVJ/L65-sig/mce/$dsid/*.root; do
    string=$(echo "$file" | rev | cut -d"/" -f3 | rev | cut -d"/" -f3) a=$(echo "$file" | cut -d"." -f3)
    name=$(echo "$file" | rev | cut -d"/" -f4 | rev | cut -d"-" -f2)
    ./Analysisscript_abcd $dsid $wtmce $file $string $a $name
done

echo "Finished running..."
